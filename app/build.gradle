plugins {
    id 'com.android.application'
    id 'kotlin-android'
    id 'kotlin-android-extensions'
    id 'kotlin-kapt'
    id 'dagger.hilt.android.plugin'
    id 'com.google.gms.google-services'
    id 'com.google.firebase.crashlytics'
}

android {
    compileSdkVersion 32
    buildToolsVersion "30.0.3"

    dataBinding.enabled = true
    defaultConfig {
        applicationId "com.lecture_list"
        minSdkVersion 23
        targetSdkVersion 32
        versionCode 1
        versionName "1.0"

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    def room_version = "2.4.1"
    def hilt_version = "2.38.1"
    def okhttp_version = "4.9.1"
    def moshi_version = '1.12.0'
    def retrofit_version = '2.9.0'

    // https://youtrack.jetbrains.com/issue/KT-33248
    // noinspection DifferentStdlibGradleVersion
    implementation "org.jetbrains.kotlin:kotlin-stdlib:$kotlin_version"
    implementation 'androidx.core:core-ktx:1.7.0'
    implementation 'androidx.appcompat:appcompat:1.4.1'
    implementation 'com.google.android.material:material:1.5.0'
    implementation 'androidx.constraintlayout:constraintlayout:2.1.3'
    testImplementation 'junit:junit:4.13.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'

    // Rx
    implementation "io.reactivex.rxjava3:rxjava:3.1.1"
    implementation "io.reactivex.rxjava3:rxkotlin:3.0.1"
    implementation "io.reactivex.rxjava3:rxandroid:3.0.0"
    implementation 'com.jakewharton.rxrelay3:rxrelay:3.0.1'

    // logger
    implementation 'com.jakewharton.timber:timber:4.7.1'

    // Room
    implementation "androidx.room:room-runtime:$room_version"
    kapt "androidx.room:room-compiler:$room_version"
    testImplementation "androidx.room:room-testing:$room_version"
    implementation "androidx.room:room-rxjava3:$room_version"

    // Hilt
    implementation "com.google.dagger:hilt-android:$hilt_version"
    kapt "com.google.dagger:hilt-android-compiler:$hilt_version"

    // Material
    implementation "com.google.android.material:material:1.5.0"

    // Firebase
    implementation platform('com.google.firebase:firebase-bom:27.1.0')
    implementation 'com.google.firebase:firebase-crashlytics-ktx:18.2.7'
    implementation 'com.google.firebase:firebase-analytics-ktx:20.0.2'

    // okhttp
    implementation "com.squareup.okhttp3:okhttp:$okhttp_version"
    testImplementation "com.squareup.okhttp3:mockwebserver:$okhttp_version"
    implementation "com.squareup.okhttp3:logging-interceptor:$okhttp_version"

    // moshi
    implementation "com.squareup.moshi:moshi:$moshi_version"
    implementation "com.squareup.moshi:moshi-kotlin:$moshi_version"

    // retrofit
    implementation "com.squareup.retrofit2:retrofit:$retrofit_version"
    implementation "com.squareup.retrofit2:converter-moshi:$retrofit_version"
    implementation "com.github.akarnokd:rxjava3-retrofit-adapter:3.0.0"

    // picasso
    implementation 'com.squareup.picasso:picasso:2.8'

    implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.1.0'

    // leak
    debugImplementation 'com.squareup.leakcanary:leakcanary-android:2.7'
}